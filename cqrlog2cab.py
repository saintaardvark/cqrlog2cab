#!/usr/bin/env python3

import configparser
import datetime as dt
import logging
from pathlib import Path
import sys

import cabrillo
import click
import pyodbc

from cqrlog2cab.config import Cqrlog2cabConfig, Contest
from cqrlog2cab.exchange import ExchangeBuilder, CqwwBuilder, RemarksBuilder, Rst_RemarksBuilder, CountyBuilder

# Logger configuration

LOGGER = logging.getLogger('__name__')
CH = logging.StreamHandler()
CH.setLevel(logging.INFO)
LOG_FORMAT = '%(levelname)s: %(message)s'
FORMATTER = logging.Formatter(LOG_FORMAT)
CH.setFormatter(FORMATTER)
LOGGER.addHandler(CH)


# Note:  As of Fedora 33, the following settings no longer work:
#
# DB_TYPE = 'MariaDB'
# DB_ADDR = '::1'
#
# I get this error:
# pyodbc.Error: ('HY000', 'The driver did not supply an error!')
#
# Switching to these settings does work:
DB_TYPE = 'MySQL'
DB_ADDR = '127.0.0.1'
#
# ಠಿ_ಠ

DB_NAME = 'cqrlog001'
DB_PORT = '64000'
DB_UID = None
DB_PASS = None

EXIT_DB_PROBLEM = 1
EXIT_QUERY_PROBLEM = 2
EXIT_NO_CONTEST_OR_QUERY = 3
EXIT_INCOMPLETE_CONTEST = 4

CONTEST_QUERY = "where qsodate >= '2019-01-11' AND qsodate <= '2019-01-13' AND remarks = 'NA QSO'"
SOAPBOX = ['Thanks for a great contest!']
CONFIG_FILE = str(Path.home()) + '/.cqrlog2cab.ini'
POWER = 'QRP'


def toggle_debug():
    """Set the default log level to debug
    """
    # Huh, need both of these
    LOGGER.setLevel(logging.DEBUG)
    CH.setLevel(logging.DEBUG)


@click.group()
def cqrlog2cab():
    """A tool to convert QSOs logged by cqrlog to Cabrillo format.
    """
    pass


@click.command('license', short_help='Print license info')
def license():
    """Print license info
    """
    copyright = "cqrlog2cab Copyright (C) 2019 Hugh Brown"
    license = """
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    """
    print(copyright)
    print(license)


def build_connstring(type=DB_TYPE, server=DB_ADDR, port=DB_PORT, database=DB_NAME, uid=DB_UID, pwd=DB_PASS):
    """Build connection string for pyodbc
    """
    connstring = "Driver={type};Server={server};Port={port};Database={database};".format(
        type=type, server=server, port=port, database=database)
    if uid is not None or pwd is not None:
        connstring += "Uid={uid};Pwd={pwd};".format(uid=uid, pwd=pwd)

    return connstring


def build_query_for_contest(contest_query):
    """Build SQL query for contest
    """
    query = "select * from cqrlog_main " + contest_query + 'and qsodate >= DATE_SUB(NOW(),INTERVAL 6 month)'
    return query


def build_qso_datetime(row):
    """Build QSO datetime from row
    """
    qso_date = row['qsodate']
    qso_hour, qso_minute = row['time_on'].split(':')
    qso_hour = int(qso_hour)
    qso_minute = int(qso_minute)
    qso_time = dt.time(hour=qso_hour, minute=qso_minute, tzinfo=dt.timezone.utc)
    return dt.datetime.combine(qso_date, qso_time)





def row2cab(row, exchange_builder, cq_zone=False, default_report=True, band_only=True):
    """Convert row to Cabrillo format

    TODO: What I really want is a mapping between the cqrlog schema
    and the Cabrillo QSO object.
    """
    if band_only is True:
        band_or_freq = row['band']
    else:
        band_or_freq = int(row['freq']*1000)

    mode = row['mode']
    # Yep, I really want that better mapping...
    # Map cqrlog's SSB to Cabrillo's PH.
    if mode == 'SSB':
        mode = 'PH'

    qso_datetime = build_qso_datetime(row)

    try:
        de_exch, dx_exch = exchange_builder.exchange(row)
    except ValueError as e:
        LOGGER.warning("Could not build exchange for QSO - setting to empty")
        LOGGER.warning(e, row)
        de_exch, dx_exch = "", ""
    dx_call = row['callsign']
    try:
        qso = cabrillo.QSO(band_or_freq,
                           mode,
                           qso_datetime,
                           'VA7UNX',
                           dx_call,
                           de_exch=de_exch,
                           dx_exch=dx_exch)
    except (cabrillo.errors.InvalidQSOException, UnboundLocalError) as e:
        LOGGER.warning("Could not construct QSO -- not adding it to list!")
        LOGGER.warning(e, row)
        return None
    return qso


def build_cabrillo_log(qsos=None,
                       config=None,
                       contest_settings=None,
                       power=POWER,
                       location=None,
                       assisted=False,
                       band='ALL',
                       transmitter='ONE',
                       mode='CW'):
    """Build cabrillo log from qsos and config, then return it
    """
    if assisted is False:
        category_assisted = "NON-ASSISTED"
    else:
        category_assisted = "ASSISTED"

    cab_log = cabrillo.Cabrillo(qso=qsos,
                                soapbox=SOAPBOX,
                                contest=contest_settings.contest_identifier,
                                name=config.user['name'],
                                callsign=config.user['callsign'],
                                email=config.user['email'],
                                address=[config.user['address']],
                                address_city=config.user['address_city'],
                                address_state_province=config.user['address_state_province'],
                                address_country=config.user['address_country'],
                                category_power=power,
                                location=location,
                                category_operator=config.user['operator'],
                                category_assisted=category_assisted,
                                category_mode=mode,
                                category_band=band,
                                category_transmitter=transmitter)
    return cab_log


def build_conn():
    """Build and return ODBC connection object
    """
    conn = None
    connstring = build_connstring()
    try:
        conn = pyodbc.connect(connstring)
    except pyodbc.Error as e:
        LOGGER.error('Error connecting to cqrlog MariaDB server. Is it running?')
        LOGGER.error('Exception: %s', e)
        sys.exit(EXIT_DB_PROBLEM)

    return conn


def fetch_query_results_as_dict_array(conn=None, query=None):
    """Run SQL query and return results as array of dicts:

    [ { "column_1": "result_1", "column_2": "result_2"},
      { "column_1": "result_3", "column_2": "result_4"},
      ...
    ]
    """
    results = []
    cursor = conn.cursor()
    try:
        cursor.execute(query)
    except pyodbc.ProgrammingError as e:
        LOGGER.error("Problem running database query!")
        LOGGER.error(e, "query = {}".format(query))
        sys.exit(EXIT_QUERY_PROBLEM)
    desc = cursor.description
    columns = [column[0] for column in cursor.description]
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))

    return results


def read_config(config_file=CONFIG_FILE):
    """Read config file, if present, and return configuration
    """
    config = configparser.ConfigParser()
    config.read(config_file)
    return config


def find_contest(contest, config):
    """Look for and return contest section of config.

    If not present, raise an exception
    """
    return config.contests[contest]


def list_all_contests(config):
    """Print a list of all configured contests
    """
    for c in config.configured_contests():
        print("{}".format(c))


def find_exchange_builder(exchange):
    """Find and return exchange builder
    """
    if exchange == 'cqww':
        return CqwwBuilder()
    elif exchange == 'remarks':
        return RemarksBuilder()
    elif exchange == 'rst_remarks':
        return Rst_RemarksBuilder()
    elif exchange == 'county':
        return CountyBuilder()
    else:
        LOGGER.warning("Can't find exchange builder for {}, going with default".format(
            exchange))
        return ExchangeBuilder()


@click.command('query', short_help='Run query against database and print results')
@click.option('--contest_query',
              default=CONTEST_QUERY,
              help='Query to select contest results. Must begin with `WHERE ...`')
@click.option('--contest',
              help='Use the query already configured for this contest in the configuration file.')
@click.option('--debug/--no-debug', help='Print debug info', default=False)
def query(contest_query, contest, debug):
    """Run query against database and print results.

    Useful for debugging.
    """
    # config = read_config()
    config = Cqrlog2cabConfig(config_file=CONFIG_FILE)
    conn = build_conn()

    if contest is not None:
        try:
            contest_settings = find_contest(contest, config)
            query = build_query_for_contest(contest_settings.contest_query)
        except KeyError as e:
            LOGGER.error('Cannot find contest or query: %s', e)
            sys.exit(EXIT_NO_CONTEST_OR_QUERY)

    else:
        query = build_query_for_contest(contest_query)

    results = fetch_query_results_as_dict_array(conn=conn, query=query)
    for row in results:
        print(row)


@click.command('print_log', short_help='Print Cabrillo log')
@click.option('--contest_query',
              default=CONTEST_QUERY,
              help='Query to select contest results. Must begin with `WHERE ...`')
@click.option('--power',
               default=POWER,
               help='One of QRP, LOW, or HIGH.')
@click.option('--location',
              help='State, section or ID, depending on contest.')
@click.option('--assisted/--no-assisted',
              default=False,
              help='ASSISTED or NON-ASSISTED.')
@click.option('--band',
              default='ALL',
              help='40M, 20M, 30M... or ALL.')
@click.option('--mode',
              default='CW',
              help='CW, RTTY or SSB.')
@click.option('--transmitter',
              help='ONE, TWO or UNLIMITED.')
@click.option('--exchange',
              help='Function to build exchange. Options: cqww. Leave blank for default.',
              default=None)
@click.option('--band-only/--no-band-only',
              help='Only show the band in the log, rather than the frequency',
              default=True)
@click.option('--contest',
              help='Use options for contest specified in config file',
              default=None)
@click.option('--debug/--no-debug', help='Print debug info', default=False)
def print_log(contest_query, power, location, assisted, band, mode,
              transmitter, exchange, band_only, contest, debug):
    """Connect to cqrlog database, gather QSOs, and print Cabrillo log.
    """
    # config = read_config()
    config = Cqrlog2cabConfig(config_file=CONFIG_FILE)
    conn = build_conn()

    if debug is True:
        toggle_debug()

    if contest is not None:
        try:
            contest_settings = find_contest(contest, config)
        except KeyError as e:
            LOGGER.warning("Can't find that contest :-( Try one of these:\n")
            list_all_contests(config)
            sys.exit(EXIT_NO_CONTEST_OR_QUERY)

        LOGGER.debug(contest_settings)
        try:
            contest_query = contest_settings.contest_query
            exchange = contest_settings.exchange
            mode = contest_settings.mode
            band_only = contest_settings.band_only
        except KeyError as e:
            LOGGER.error('Missing key(s) from contest settings: %s', e)
            sys.exit(EXIT_INCOMPLETE_CONTEST)

    qsos = []

    query = build_query_for_contest(contest_query)
    results = fetch_query_results_as_dict_array(conn=conn, query=query)

    exchange_builder = find_exchange_builder(exchange)

    for row in results:
        qsos.append(row2cab(row, exchange_builder, band_only=band_only))

    cab_log = build_cabrillo_log(qsos=qsos,
                                 config=config,
                                 contest_settings=contest_settings,
                                 power=power,
                                 location=location,
                                 assisted=assisted,
                                 band=band,
                                 mode=mode)

    print(cab_log.write_text())


cqrlog2cab.add_command(print_log)
cqrlog2cab.add_command(license)
cqrlog2cab.add_command(query)

if __name__ == '__main__':
    cqrlog2cab()
