run_tests:
	pytest -v --cov=cqrlog2cab

test_default:
	./cqrlog2cab.py print_log \
		--power QRP \
		--location ON \
		--assisted NON-ASSISTED \
		--band 20M \
		--transmitter ONE

test_cq:
	./cqrlog2cab.py print_log \
		--contest_query 'WHERE award="CQWWSSB"' \
		--power 'LOW' \
		--location 'BC' \
		--assisted NON-ASSISTED \
		--band 20M

query_cq:
	./cqrlog2cab.py query \
		--contest_query 'WHERE award="CQWWSSB"'

# On Fedora, run `dnf install -y mariadb-connector-odbc unixODBC-devel`
install:
	python3 -mvirtualenv venv
	source venv/bin/activate && pip install -r requirements.txt
