"""Utility functions for cqrlog2cab
"""

def get_signal_report(row, default=True, sent=True):
    """Return signal report.

    Returns:
    - if default is True:
        - 59 if row['mode'] is phone/ssb
        - 599 if row['mode'] is CW
    - if default is False:
        - row['rst_s'] if sent is True
        - row['rst_r'] if sent is False

    FIXME: I don't like the way we ignore sent argument unless default
    is False.  It feels ugly.
    """
    if default is True:
        if row['mode'] == 'SSB':
            return '59'
        elif row['mode'] == 'CW':
            return '599'
    else:
        if sent is True:
            return str(row['rst_s'])
        else:
            return str(row['rst_r'])
