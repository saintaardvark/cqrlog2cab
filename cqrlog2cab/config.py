"""Module for config file classes
"""

import configparser


class Cqrlog2cabConfig():
    """Class for cqrlog2cab configuration
    """

    # TODO: See if this could be simplified by subclassing configparser  # pylint: disable=fixme
    def __init__(self, config_file=None):
        """Initialize this class
        """
        if config_file is None:
            pass

        self._config = configparser.ConfigParser()
        self._config.read(config_file)

        self.user = self._config['user']

        self.contests = {}
        for section in self._config.keys():
            if self.__is_contest(section):
                contest_name = section.replace('contest_', '')
                contest_dict = dict(self._config[section].items())
                self.contests[contest_name] = Contest(contest_dict)

    def __is_contest(self, section):  # pylint: disable=no-self-use
        """Determine if a section is a function or not
        """
        return section.startswith("contest_")

    def configured_contests(self):
        """Return list of contests that are in the config file
        """
        return self.contests.keys()


class Contest():
    """Class for cqrlog2cab contest settings
    """

    _defaults = {
        'contest_query': "WHERE award = 'ARRL DX'",
        'soapbox': 'Thanks for a great contest!',
        'contest_identifier': 'ARRL-DX-CW',
        'mode': 'CW',
        'exchange': 'remarks',
        'band_only': True,
        'include_rst': True,
        'query_time_limit_days': 30,
    }

    def __init__(self, items):
        """Initialize this class
        """
        self._values = {**self._defaults, **items}

    # TODO: There has to be a better way to do this...  # pylint: disable=fixme

    @property
    def contest_query(self):
        """Return contest query
        """
        return self._values['contest_query']

    @property
    def soapbox(self):
        """Return contest query
        """
        return self._values['soapbox']

    @property
    def contest_identifier(self):
        """Return contest query
        """
        return self._values['contest_identifier']

    @property
    def mode(self):
        """Return contest query
        """
        return self._values['mode']

    @property
    def exchange(self):
        """Return contest query
        """
        return self._values['exchange']

    @property
    def band_only(self):
        """Return contest query
        """
        return self._values['band_only']

    @property
    def include_rst(self):
        """Return contest query
        """
        return self._values['include_rst']

    @property
    def query_time_limit_days(self):
        """Return contest query
        """
        return self._values['query_time_limit_days']
