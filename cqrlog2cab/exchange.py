"""Classes for exchange builders
"""

import logging

from .util import get_signal_report


LOGGER = logging.getLogger(__name__)


# A note about exchange builder functions: because of the way QSOs are
# built by the Cabrillo library, the exchange builder function needs
# to return the de_exch and the dx_exch as arrays.  Example:
#
# de_exch = "Hi Bob"
# dx_exch = "Hi Jane"
# return [de_exch], [dx_exch]

class ExchangeBuilder():
    """Class for functions to build contest exchanges
    """

    def __init__(self):
        pass

    def exchange(self, row):
        """Default method for building an exchange
        """
        default_report = get_signal_report(row)
        de_exch = [default_report]
        dx_exch = [default_report]
        return de_exch, dx_exch


class CqwwBuilder(ExchangeBuilder):
    """Build CQWW-specific exchange
    """

    def exchange(self, row, de_zone='03'):
        """Build exchange
        """
        default_report = get_signal_report(row)
        de_exch = [default_report, de_zone]
        dx_exch = [default_report, str(row['waz'])]
        return de_exch, dx_exch

class CountyBuilder(ExchangeBuilder):
    """Build exchange from county and state
    """
    def exchange(self, row, de_spc='BC'):
        """Build exchange
        """
        default_report = get_signal_report(row)
        de_exch = [default_report, de_spc]
        dx_exch = [default_report, "{}{}".format(row['county'], row['state'])]
        return de_exch, dx_exch


class Rst_RemarksBuilder(ExchangeBuilder):
    def exchange(self, row):
        """Build exchange from RST and comment

        Note: expects "remarks" field to be formatted like so:

        r'.*Exchange: (\S+) (.+)$'

        where the first group is the DE (local) exchange, and the second
        is the DX (remote) exchange. Any text up to and including
        "Exchange: " is discarded.  Only one split is done of the text
        after "Exchange".

        If exchange can't be built successfully, warn and set exchange to
        `FIXME` so that the user can fix things.
        """
        remarks = row['remarks']
        default_report = get_signal_report(row)

        try:
            # Get everything following "Exchange: "...
            exch = remarks.split('Exchange: ')[1]
            # ...and split just once on whitespace:
            de_exch, dx_exch = exch.split(maxsplit=1)
        except IndexError as e:
            LOGGER.warning("Can't build exchange for this QSO! Setting exchange to `FIXME`")
            LOGGER.warning(e, row)
            de_exch, dx_exch = "FIXME", "FIXME"
        return [default_report, de_exch], [default_report, dx_exch]


class RemarksBuilder(ExchangeBuilder):
    """Build exchange from remarks
    """

    def exchange(self, row):
        """Build exchange from comment

        Note: expects "remarks" field to be formatted like so:

        r'.*Exchange: (\S+) (.+)$'

        where the first group is the DE (local) exchange, and the second
        is the DX (remote) exchange. Any text up to and including
        "Exchange: " is discarded.  Only one split is done of the text
        after "Exchange".

        If exchange can't be built successfully, warn and set exchange to
        `FIXME` so that the user can fix things.
        """
        remarks = row['remarks']

        try:
            # Get everything following "Exchange: "...
            exch = remarks.split('Exchange: ')[1]
            # ...and split just once on whitespace:
            de_exch, dx_exch = exch.split(maxsplit=1)
        except IndexError as e:
            LOGGER.warning("Can't build exchange for this QSO! Setting exchange to `FIXME`")
            LOGGER.warning(e, row)
            de_exch, dx_exch = "FIXME", "FIXME"
        return [de_exch], [dx_exch]
