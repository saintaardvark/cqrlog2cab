from cqrlog2cab.util import get_signal_report


SSB_ROW = {'mode': 'ssb', 'rst_s': '48', 'rst_r': '37'}
CW_ROW = {'mode': 'cw', 'rst_s': '489', 'rst_r': '379'}

def test_get_signal_report_ssb_default_true():
    """Test get_signal_report ()
    """
    assert get_signal_report(row=SSB_ROW, default=True) == '59'


def test_get_signal_report_cw_default_true():
    """Test get_signal_report ()
    """
    assert get_signal_report(row=CW_ROW, default=True) == '599'
