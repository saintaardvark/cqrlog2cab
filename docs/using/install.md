# Installation

cqrlog2cab uses Python 3.  Install dependencies like so:

```
pip install -r requirements.txt
```

You'll also need some other libraries -- MariaDB devel, ODBC (MariaDB
but also MySQL -- see note ahead about `pyodbc.Error: ('HY000', 'The
driver did not supply an error!')`), and a couple others.  For Fedora
33:

```
sudo dnf install -y mariadb-devel mysql-connector-odbc unixODBC-devel
```
