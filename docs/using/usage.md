# Usage

Make sure cqrlog is running; this program connects to its database to
do its work.

## Configuration

Create a config file at `~/cqrlog2cab.ini` that looks like this:

```
[user]
name = Hugh Brown
callsign = VA7UNX
email = va7unx@members.fsf.org
address = 24 Sussex Drive
address_city = Ottawa
address_state_province = ON
address_postalcode = K1A 0A2
address_country = Canada

# Optional contest-specific sections

# The name of a contest section must be in the form "contest_[name]".
# You can then specify these settings by using "--contest [name]".
[contest_cqww_phone]
#
# The keys are the arguments you'd supply to the CLI, with the
# following exceptions:
#
# - hyphens become underscores ("foo-bar" becomes "foo_bar")
#
# - Any arguments in the form "--foo/--no-foo" become:
#
#     foo = True   # Same as "--foo"
#     foo = False  # Same as "--no-foo"
#
# One example where this currently applies is the argument
# "--band-only/--no-band-only":
#
#   band_only = True   # Same as "--band-only"
#   band_only = False  # Same as "--no-band-only"
#
# - Not all arguments may be currently supported.

contest_query = WHERE award="CQWWSSB" # This is the name I put in the award section for this contest.
mode = SSB
band_only = False
exchange = cqww

# Multiple contest sections are supported.
[contest_cqww_cw]
contest_query = WHERE award="CQWW"
mode = CW
band-only = False
exchange = cqww
```

## Command: `print_log`

```
Usage: cqrlog2cab.py print_log [OPTIONS]

  Connect to cqrlog database, gather QSOs, and print Cabrillo log.

Options:
  --contest_query TEXT          Query to select contest results. Must begin
                                with `WHERE ...`
  --power TEXT                  One of QRP, LOW, or HIGH.
  --location TEXT               State, section or ID, depending on contest.
  --assisted / --no-assisted    ASSISTED or NON-ASSISTED.
  --band TEXT                   40M, 20M, 30M... or ALL.
  --mode TEXT                   CW, RTTY or SSB.
  --transmitter TEXT            ONE, TWO or UNLIMITED.
  --exchange TEXT               Function to build exchange. Options: cqww.
                                Leave blank for default.
  --band-only / --no-band-only  Only show the band in the log, rather than the
                                frequency
  --contest TEXT                Use options for contest specified in config
                                file
  --help                        Show this message and exit.
```

Example using the default query:
```
$ ./cqrlog2cab.py print_log \
	--power QRP \
	--location ON \
	--assisted NON-ASSISTED \
	--band 20M \
	--transmitter ONE

START-OF-LOG: 3.0
CALLSIGN: VA7UNX
CATEGORY-ASSISTED: NON-ASSISTED
CATEGORY-BAND: 20M
CATEGORY-OPERATOR: SINGLE-OP
CATEGORY-POWER: QRP
CATEGORY-TRANSMITTER: ONE
CREATED-BY: cabrillo (Python)
EMAIL: va7unx@members.fsf.org
LOCATION: ON
NAME: Hugh Brown
ADDRESS: 24 Sussex Drive
ADDRESS-CITY: Ottawa
ADDRESS-STATE-PROVINCE: ON
ADDRESS-COUNTRY: Canada
SOAPBOX: Thanks for a great contest!
QSO: 15M CW 2019-01-12 0000 VA7UNX  VE7SAR
QSO: 20M CW 2019-01-12 0000 VA7UNX  K4XL
QSO: 20M CW 2019-01-12 0000 VA7UNX  K3MD
QSO: 20M CW 2019-01-12 0000 VA7UNX  K4XY
QSO: 20M CW 2019-01-12 0000 VA7UNX  KC4D
QSO: 20M CW 2019-01-12 0000 VA7UNX  NO5W
QSO: 20M CW 2019-01-12 0000 VA7UNX  W0ZA
QSO: 20M CW 2019-01-12 0000 VA7UNX  W9XT
QSO: 20M CW 2019-01-12 0000 VA7UNX  K6NR
QSO: 20M CW 2019-01-12 0000 VA7UNX  K5WE
QSO: 20M CW 2019-01-12 0000 VA7UNX  VE7KW
END-OF-LOG:
```

Example using a specified query:

```
$ ./cqrlog2cab.py print_log --contest_query 'WHERE award="CQWWSSB"' --no-band-only --exchange cqww
START-OF-LOG: 3.0
CALLSIGN: VA7UNX
CATEGORY-ASSISTED: NON-ASSISTED
CATEGORY-MODE: CW
CATEGORY-OPERATOR: SINGLE-OP
CATEGORY-POWER: QRP
CATEGORY-TRANSMITTER: ONE
CREATED-BY: cabrillo (Python)
EMAIL: va7unx@members.fsf.org
NAME: Hugh Brown
ADDRESS: 24 Sussex Drive
ADDRESS-CITY: Ottawa
ADDRESS-STATE-PROVINCE: ON
ADDRESS-COUNTRY: Canada
SOAPBOX: Thanks for a great contest!
QSO: 14151 PH 2019-10-27 2155 VA7UNX 59 03 N7DD 59 3
QSO: 14153 PH 2019-10-27 2157 VA7UNX 59 03 KC7V 59 3
QSO: 14173 PH 2019-10-27 2157 VA7UNX 59 03 NV9L 59 4
QSO: 14177 PH 2019-10-27 2159 VA7UNX 59 03 N2IC 59 4
QSO: 14223 PH 2019-10-27 2212 VA7UNX 59 03 N6KN 59 3
QSO: 14233 PH 2019-10-27 2216 VA7UNX 59 03 JI2ZEY 59 25
QSO: 14235 PH 2019-10-27 2218 VA7UNX 59 03 JJ0JML 59 25
QSO: 14262 PH 2019-10-27 2224 VA7UNX 59 03 JA7QVI 59 25
END-OF-LOG:
```

Example using a pre-specified contest:

```
$ ./cqrlog2cab.py print_log --contest cqww_phone
[same as above]
```

## Command: `query`

```
Usage: cqrlog2cab.py query [OPTIONS]

  Run query against database and print results.

  Useful for debugging.

Options:
  --contest_query TEXT  Query to select contest results. Must begin with
                        `WHERE ...`
  --contest TEXT        Use the query already configured for this contest
                        in the configuration file.
  --help                Show this message and exit.

```

Runs a query against the database and prints the results.  The format
isn't terribly pretty, but should help with debugging.

Example:

```
./cqrlog2cab.py query \
	--contest_query 'WHERE award="CQWWSSB"'
{'id_cqrlog_main': 351, 'qsodate': datetime.date(2019, 10, 27), 'time_on': '21:55', 'time_off': '21:55', 'callsign': 'N7DD', 'freq': Decimal('14.1510'), 'mode': 'SSB', 'rst_s': '59', 'rst_r': '59', 'name': '', 'qth': '', 'qsl_s': 'B', 'qsl_r': '', 'qsl_via': '', 'iota': '', 'pwr': '15', 'itu': 6, 'waz': 3, 'loc': 'DM42LJ', 'my_loc': 'FN25dk', 'county': '', 'award': 'CQWWSSB', 'remarks': 'Park-portable doublet', 'adif': 291, 'band': '20M', 'qso_dxcc': 0, 'profile': 8, 'idcall': 'N7DD', 'state': 'AZ', 'lotw_qslsdate': datetime.date(2019, 10, 27), 'lotw_qslrdate': None, 'lotw_qsls': 'Y', 'lotw_qslr': '', 'cont': 'NA', 'qsls_date': '2019-10-28', 'qslr_date': None, 'club_nr1': '', 'club_nr2': '', 'club_nr3': '', 'club_nr4': '', 'club_nr5': '', 'eqsl_qsl_sent': 'Y', 'eqsl_qslsdate': datetime.date(2019, 10, 27), 'eqsl_qsl_rcvd': '', 'eqsl_qslrdate': None, 'rxfreq': None, 'satellite': '', 'prop_mode': ''}
[snip]
```

## Command: `license`

Print the license for this project (GPLv3).

# Assumptions

- cqrlog's copy of MariaDB is listening on ::1, port 64000
- no authentication required
- database is named `cqrlog001`
- the query for logs has a hardcoded limit of six months, to avoid
  pulling in *last* year's results
- there's a lot of hard-coded stuff at the moment, so it also assumes
  you're me. :-)

## Common errors

## `pyodbc.Error: ('HY000', 'The driver did not supply an error!')`

I came across this after upgrading to Fedora 33.  I'm not sure what's
going on here, but changing the ODBC connector from MariaDB to MySQL
worked.  See the comments for `DB_TYPE` and `DB_ADDR` in
cqqlog2cab.py.
