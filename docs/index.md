# Welcome to cqrlog2cab's documentation!

cqrlog2cab is a Small but Helpful(tm) utility to grab data from cqrlog's database,
then print it out in Cabrillo format suitable for contest submissions.

# Site contents

```{toctree}
---
maxdepth: 1
caption: Usage
---
using/install.md
using/usage.md
using/license.md
```
